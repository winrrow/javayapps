package jyaa2018.grupo15.tests;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;

import jyaa2018.grupo15.dao.impl.FactoryDaoImpl;
import jyaa2018.grupo15.dao.impl.RutaDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioAdministradorDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioComunDaoImpl;
import jyaa2018.grupo15.models.Ruta;
import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioAdministrador;
import jyaa2018.grupo15.models.UsuarioComun;

class testsCase {
	FactoryDaoImpl fact = new FactoryDaoImpl();

	@Test
	void crearUsuarioTest() {
		// List<Ruta> misRutas;
		// List<Ruta> rutasVisitadas;
		Date fechaNacimiento = new Date();
		UsuarioComun u = new UsuarioComun(new Date(), fechaNacimiento, "prueba1", "prueba1", "user1", "unaPassword",
				"pepe@hotmail.com", null, null);
		UsuarioComunDaoImpl d = (UsuarioComunDaoImpl) fact.getUsuarioComunDao();
		d.save(u);
		d.deleteById(u.getId());
		Usuario usuario = d.getItem(u.getId());
		Assert.assertNotNull(u);
		// Assert.assertEquals("pepe@hotmail.com", usuario.getEmail());
	}

	@Test
	void deshabilitarUsuarioTest() {
		Date fechaNacimiento = new Date();
		UsuarioComun u = new UsuarioComun(new Date(), fechaNacimiento, "nombre", "apellido", "usuario", "password",
				"anto@hotmail.com", null, null);
		UsuarioComunDaoImpl d = (UsuarioComunDaoImpl) fact.getUsuarioComunDao();
		d.save(u);		
		Usuario usuario = d.getItem(u.getId());
		Assert.assertEquals(false, usuario.getHabilitado());
	}

	@Test
	void deshabilitarUsuarioAdmin() {
		UsuarioAdministrador u = new UsuarioAdministrador(new Date(), true, "Mati", "hola", "mati@hotmail.com", "Mati",
				"Prueba", new Date(04 / 04 / 1990));

		UsuarioAdministradorDaoImpl d = (UsuarioAdministradorDaoImpl) fact.getUsuarioAdministradorDao();
		d.save(u);
		u.setHabilitado(false);
		d.delete(u); // era DeshabilitarUsuarioPorUsuario
		Assert.assertEquals(false, u.getHabilitado());
	}

	@Test
	void crearRuta() {
		Ruta unaRuta = new Ruta("Jujuy", null, null, null, null, null, null, new Date(06 / 06 / 2018), null, null);
		RutaDaoImpl rutaDao = (RutaDaoImpl) fact.getRutaDao();
		rutaDao.save(unaRuta);
		Ruta ruta = rutaDao.getItem(unaRuta.getId());
		Assert.assertNotNull(ruta);
		Assert.assertEquals("Jujuy", ruta.getNombre());
	}

}
