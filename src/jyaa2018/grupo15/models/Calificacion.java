package jyaa2018.grupo15.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Calificacion")
public class Calificacion implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private UsuarioComun usuario;
	private Ruta ruta;
	private Integer puntaje;
	
	public Calificacion(UsuarioComun usuario, Ruta ruta, Integer puntaje) {
		super();
		this.usuario = usuario;
		this.ruta = ruta;
		this.puntaje = puntaje;		
	}

	public Calificacion() {
	};

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioComun getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioComun usuario) {
		this.usuario = usuario;
	}

	public Integer getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Integer puntaje) {
		this.puntaje = puntaje;
	}

	public Ruta getRuta() {
		return ruta;
	}

	public void setRuta(Ruta ruta) {
		this.ruta = ruta;
	}
}
