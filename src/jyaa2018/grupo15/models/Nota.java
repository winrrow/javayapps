package jyaa2018.grupo15.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Nota")
public class Nota implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6780017545035888954L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private UsuarioComun usuario;
	private Ruta ruta;
	private TipoNota tipoNota;
	private String textoNota;
	

	public Nota() {
	};

	public Nota(UsuarioComun usuario, Ruta ruta, Integer puntaje, TipoNota tipoNota) {
		super();
		this.usuario = usuario;
		this.ruta = ruta;
		this.tipoNota = tipoNota;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioComun getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioComun usuario) {
		this.usuario = usuario;
	}

	public Ruta getRuta() {
		return ruta;
	}

	public void setRuta(Ruta ruta) {
		this.ruta = ruta;
	}

	public TipoNota getTipoNota() {
		return tipoNota;
	}

	public void setTipoNota(TipoNota tipoNota) {
		this.tipoNota = tipoNota;
	}

	public String getTextoNota() {
		return textoNota;
	}

	public void setTextoNota(String textoNota) {
		this.textoNota = textoNota;
	}
}
