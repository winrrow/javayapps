package jyaa2018.grupo15.models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class UsuarioAdministrador extends Usuario {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4069148358223123553L;
	private String nombre;
	private String apellido;
	private Date fechaDeNacimiento;

	public UsuarioAdministrador(Date fechaAlta, Boolean habilitado, String nombreUsuario, String password, String email,
			String nombre, String apellido, Date fechaDeNacimiento) {
		//this.id = id;
		this.fechaAlta = fechaAlta;
		this.nombreUsuario = nombreUsuario;
		this.password = password;
		this.email = email;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaDeNacimiento = fechaDeNacimiento;
		this.habilitado=true;
	}

	public UsuarioAdministrador() {
	};

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public void borrarUsuario(Usuario usuario) {

	}

	public List<Usuario> listarUsuarios() {
		return null;
	}

	public void deshabilitarUsuario(Usuario usuario) {

	}

	public void habilitarUsuario(Usuario usuario) {

	}

	public void agregarActividad(Actividad actividad) {

	}

	// Pongo actividad o id?
	public void modificarActividad(Actividad actividad, String nombre) {

	}

	public boolean borrarActividad(Actividad actividad) {
		return true;
	}
}
