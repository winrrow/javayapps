package jyaa2018.grupo15.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Usuario")
public abstract class Usuario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -462997472824714225L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	protected Date fechaAlta;
	protected String nombreUsuario;
	// protected String nombre; SOLO ADMIN
	// protected String apellido; SOLO ADMIN
	protected String password;
	protected String email;
	protected String perfil;
	protected Boolean habilitado;
	// protected Date fechaDeNacimiento; SOLO ADMIN

	public Usuario() {
		this.habilitado = true;
		this.fechaAlta= new Date();
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombre) {
		this.nombreUsuario = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
}
