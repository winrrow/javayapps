package jyaa2018.grupo15.models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class UsuarioComun extends Usuario {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1315034565122522100L;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL)
	private List<Ruta> rutasVisitadas;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL)
	private List<Ruta> misRutas;

	public UsuarioComun(Date fechaAlta, Date fechaDeNacimiento, String nombre, String apellido, String nombreUsuario, String password, String email, List<Ruta> rutasVisitadas,
			List<Ruta> misRutas) {
		this.fechaAlta = fechaAlta;
		/*this.fechaDeNacimiento = fechaDeNacimiento;
		this.nombre = nombre;
		this.apellido = apellido;*/
		this.nombreUsuario = nombreUsuario;
		this.password = password;
		this.email = email;
		this.rutasVisitadas = rutasVisitadas;
		this.misRutas = misRutas;
		this.habilitado = true;

	}

	public UsuarioComun() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Ruta> getRutasVisitadas() {
		return rutasVisitadas;
	}

	public void setRutasVisitadas(List<Ruta> rutasVisitadas) {
		this.rutasVisitadas = rutasVisitadas;
	}

	public List<Ruta> getMisRutas() {
		return misRutas;
	}

	public void setMisRutas(List<Ruta> misRutas) {
		this.misRutas = misRutas;
	}

	public void agregarRuta(Ruta ruta) {

	}

	public void modificarRuta(Ruta ruta) {

	}

	public void eliminarRuta(Ruta ruta) {

	}

	public void agregarCalificacionARuta(Ruta ruta) {

	}
}
