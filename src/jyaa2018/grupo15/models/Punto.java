package jyaa2018.grupo15.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Punto")
public class Punto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7609114009947775465L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private double coordenadaX;
	private double coordenadaY;
	private Boolean habilitado;

	public Punto(double p1, double p2) {
		// asignamos esos valores
		coordenadaX = p1;
		coordenadaY = p2;
		this.habilitado = true;
	}

	public double distancia(int x, int y) {
		// este método recibe como parametro las coordenadas del segundo punto
		double D;
		D = Math.sqrt((coordenadaX - x) * (coordenadaX - x) + (coordenadaY - y) * (coordenadaY - y));
		return D;// Retornamos el valor de la distancia
	}

	public Punto puntoMedio(int x, int y) {
		double p1, p2;
		p1 = (coordenadaX + x) / 2;
		p2 = (coordenadaY + y) / 2;
		// System.out.println("El punto medio es: "+p1+","+p2);
		return new Punto(p1, p2);
	}

	public Punto() {
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public double getCoordenadaX() {
		return coordenadaX;
	}

	public void setCoordenadaX(Integer coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public double getCoordenadaY() {
		return coordenadaY;
	}

	public void setCoordenadaY(Integer coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
