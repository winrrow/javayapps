package jyaa2018.grupo15.models;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
@Entity
@Table(name="Ruta")
public class Ruta implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8768104492064021808L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String descripcion;
    private String privacidad;
    private String formato;
    private Integer distancia;
    private String dificultad;
    private Integer tiempoEstimado;
    private Date fechaRealizacion;
    private String kml;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name ="FK_ActividadId")
    private Actividad actividad;
    // @OneToMany(cascade = CascadeType.ALL)
    // private List<Foto> foto;
    private Boolean habilitado;

    
    public Ruta(String nombre, String descripcion, String privacidad, String formato, Integer distancia,
			String dificultad, Integer tiempoEstimado, Date fechaRealizacion, String kml, Actividad actividad
			/*List<Foto> foto*/) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.privacidad = privacidad;
		this.formato = formato;
		this.distancia = distancia;
		this.dificultad = dificultad;
		this.tiempoEstimado = tiempoEstimado;
		this.fechaRealizacion = fechaRealizacion;
		this.kml = kml;
		this.actividad = actividad;
		// this.foto = foto;
		this.habilitado = true;
	}

	public Ruta() {}
    
    public Boolean getHabilitado(){
    	return habilitado;
    } 
    
    public void setHabilitado(Boolean habilitado) {
    	this.habilitado=habilitado;
    }
    
    /* public List<Foto> getFoto() {
        return foto;
    }

    public void setFoto(List<Foto> foto) {
        this.foto = foto;
    } */

    public String getKml() {
        return kml;
    }

    public void setRecorrido(String kml) {
        this.kml = kml;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrivacidad() {
        return privacidad;
    }

    public void setPrivacidad(String privacidad) {
        this.privacidad = privacidad;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public Integer getDistancia() {
        return distancia;
    }

    public void setDistancia(Integer distancia) {
        this.distancia = distancia;
    }

    public String getDificultad() {
        return dificultad;
    }

    public void setDificultad(String dificultad) {
        this.dificultad = dificultad;
    }

    public Integer getTiempoEstimado() {
        return tiempoEstimado;
    }

    public void setTiempoEstimado(Integer tiempoEstimado) {
        this.tiempoEstimado = tiempoEstimado;
    }

    public Date getFechaRealizacion() {
        return fechaRealizacion;
    }

    public void setFechaRealizacion(Date fechaRealizacion) {
        this.fechaRealizacion = fechaRealizacion;
    }

    public Double obtenerPuntajePromedio(){
        return 0.0;
    }
}
