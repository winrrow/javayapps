package jyaa2018.grupo15.helpers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConexionSingleton {

	private static EntityManager entityM;
	private static EntityManagerFactory eMFactory = Persistence.createEntityManagerFactory("miUP");

	private ConexionSingleton() {

	} // nadie puede hacer new de este

	public static EntityManager getEntityManager() {

		return entityM = eMFactory.createEntityManager();
	}


}
