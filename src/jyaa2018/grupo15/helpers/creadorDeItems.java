package jyaa2018.grupo15.helpers;

import jyaa2018.grupo15.dao.impl.FactoryDaoImpl;
import jyaa2018.grupo15.dao.impl.RutaDaoImpl;
import jyaa2018.grupo15.daoNOSEUSAMAS.FactoryDao;
import jyaa2018.grupo15.models.Ruta;

public class creadorDeItems {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FactoryDao factoryDao = new FactoryDaoImpl();

		RutaDaoImpl rutaDao = (RutaDaoImpl) factoryDao.getRutaDao();

		Ruta r = new Ruta();
		r.setNombre("Ruta nro 1");
		r.setHabilitado(true);

		rutaDao.save(r);
	}

}
