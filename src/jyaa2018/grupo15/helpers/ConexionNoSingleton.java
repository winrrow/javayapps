package jyaa2018.grupo15.helpers;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConexionNoSingleton {

	protected EntityManager entityM;
	private static EntityManagerFactory eMFactory;

	public ConexionNoSingleton() {

	}
	
	public static EntityManagerFactory getManagerFactory() {

		if (eMFactory == null) {
			eMFactory = Persistence.createEntityManagerFactory("miUP");
		}
		return eMFactory;
	}

	public EntityManager getEntityManager() {					
				
		if ((entityM == null) || (!entityM.isOpen())) {
			entityM = getManagerFactory().createEntityManager();
		}
		
		return entityM; 
	}


}