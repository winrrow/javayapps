package jyaa2018.grupo15.daoNOSEUSAMAS;

import java.util.List;

import jyaa2018.grupo15.models.Ruta;

public interface RutaDao {

	List<Ruta> getItems();

	Ruta getItem(Long id);

	void guardarRuta(Ruta r);

	void modificarRuta(Ruta r);

	void borrarRuta(Long id);
	
	List<Ruta> getRutasDeUsuario(Long id);

}
