package jyaa2018.grupo15.daoNOSEUSAMAS;

import java.util.List;

import jyaa2018.grupo15.models.Calificacion;

public interface CalificacionDao {
	
	public List<Calificacion> getItems();

	public Calificacion getItem(Long id);

	public void guardarCalificacion(Calificacion c);

	public void modificarCalificacion(Calificacion c);

	public void borrarCalificacion(Long id);
}
