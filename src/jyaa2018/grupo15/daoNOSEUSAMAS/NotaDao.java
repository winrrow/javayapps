package jyaa2018.grupo15.daoNOSEUSAMAS;

import java.util.List;
import jyaa2018.grupo15.models.Nota;

public interface NotaDao {

	public List<Nota> getItems();

	public Nota getItem(Long id);

	public void guardarNota(Nota n);

	public void modificarNota(Nota n);

	public void borrarNota(Long id);
}
