package jyaa2018.grupo15.daoNOSEUSAMAS;

import java.util.List;

import jyaa2018.grupo15.models.Punto;

public interface PuntoDao {
	
	public List<Punto> getItems();

	public Punto getItem(Long id);

	public void guardarPunto(Punto p);

	public void modificarPunto(Punto p);

	public void borrarPunto(Long id);

}
