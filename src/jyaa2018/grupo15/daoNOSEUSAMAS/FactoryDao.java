package jyaa2018.grupo15.daoNOSEUSAMAS;

import jyaa2018.grupo15.dao.impl.ActividadDaoImpl;
import jyaa2018.grupo15.dao.impl.CalificacionDaoImpl;
import jyaa2018.grupo15.dao.impl.DificultadDaoImpl;
import jyaa2018.grupo15.dao.impl.NotaDaoImpl;
import jyaa2018.grupo15.dao.impl.RutaDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioAdministradorDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioComunDaoImpl;

public interface FactoryDao {

	//public ActividadDao getActividadDAO();
	
	//public CalificacionDao getCalificacionDAO();
	
	//public FotoDao getFotoDAO();
	
	//public NotaDao getNotaDao();
	
	//public PuntoDao getPuntoDao();
	
	//public RecorridoDao getRecorridoDao();
	
	//public RutaDao getRutaDao();
	
	//public UsuarioDao getUsuarioComunDao();
	
	//public UsuarioDao getUsuarioAdministradorDao();

	public ActividadDaoImpl getActividadDAO();
	
	public DificultadDaoImpl getDificultadDAO();
	
	public CalificacionDaoImpl getCalificacionDAO();
	
	//public FotoDaoImpl getFotoDAO();
	
	public NotaDaoImpl getNotaDao();
	
	// public PuntoDaoImpl getPuntoDao();
	
	//public RecorridoDaoImpl getRecorridoDao();
	
	public RutaDaoImpl getRutaDao();
	
	public UsuarioComunDaoImpl getUsuarioComunDao();
	
	public UsuarioAdministradorDaoImpl getUsuarioAdministradorDao();

	

}
