package jyaa2018.grupo15.daoNOSEUSAMAS;

import java.util.List;
import jyaa2018.grupo15.models.Actividad;

public interface ActividadDao {

	public List<Actividad> getItems();

	public Actividad getItem(Long id);

	public void guardarActividad(Actividad a);

	public void modificarActividad(Actividad a);

	public void borrarActividad(Long id);
}
