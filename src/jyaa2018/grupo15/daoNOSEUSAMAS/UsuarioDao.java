package jyaa2018.grupo15.daoNOSEUSAMAS;

import java.math.BigInteger;
import java.util.List;

import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioComun;

public interface UsuarioDao {

	List<Usuario> getItems();

	Usuario getItem(Long id);

	void guardarUsuario(Usuario u);

	void modificarUsuario(Usuario r);

	void borrarUsuario(Long id);
	
	Usuario getItem(BigInteger id);

	Usuario checkUser(String nombre, String pass);


}
