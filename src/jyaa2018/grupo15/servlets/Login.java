package jyaa2018.grupo15.servlets;

import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioComun;

/**
 * Servlet implementation class Login
 */
@WebServlet(urlPatterns = { "/Login" }, initParams = { @WebInitParam(name = "usuario1", value = "Anto"),
		@WebInitParam(name = "usuario2", value = "Joaco"), @WebInitParam(name = "usuario3", value = "Matias"),
		@WebInitParam(name = "usuario4", value = "Pepe"), @WebInitParam(name = "pass1", value = "111"),
		@WebInitParam(name = "pass2", value = "222"), @WebInitParam(name = "pass3", value = "333"),
		@WebInitParam(name = "pass4", value = "444"), @WebInitParam(name = "perfil1", value = "ADMIN"),
		@WebInitParam(name = "perfil2", value = "ADMIN"), @WebInitParam(name = "perfil3", value = "ADMIN"),
		@WebInitParam(name = "perfil4", value = "USUARIO"), @WebInitParam(name = "cantidadUsuarios", value = "4") })

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Hashtable<String, Usuario> logins;// = new Hashtable<String, Usuario>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String usuario = request.getParameter("username");
		String password = request.getParameter("password");

		Usuario logueado = new UsuarioComun();

		try {
			logueado = logins.get(usuario);
			// verifico si el usuario es valido
			if (password.equals(logueado.getPassword())) {
				// logueo satisfactorio creo la sesion y guardo datos
				HttpSession sesion = request.getSession();
				sesion.setAttribute("nombreUsuario", request.getParameter("username"));
				sesion.setAttribute("pass", request.getParameter("password"));
				if (logueado.getPerfil().equals("ADMIN")) {
					response.sendRedirect("admin-perfil.html");
				} else
					response.sendRedirect("user-perfil.html");
			} else {
				// redirecciono a la pagina de loguo
				response.sendRedirect("login.html");
			}
		} catch (Exception e) {
			// redirecciono a la pagina de loguo
			response.sendRedirect("login.html");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String usuario = request.getParameter("username");
		String password = request.getParameter("password");

		Usuario logueado = new UsuarioComun();

		try {
			logueado = logins.get(usuario);
			// verifico si el usuario es valido
			if (password.equals(logueado.getPassword())) {
				// logueo satisfactorio creo la sesion y guardo datos
				HttpSession sesion = request.getSession();
				sesion.setAttribute("nombreUsuario", request.getParameter("username"));
				sesion.setAttribute("pass", request.getParameter("password"));
				if (logueado.getPerfil().equals("ADMIN")) {
					response.sendRedirect("admin-perfil.html");
				} else
					response.sendRedirect("user-perfil.html");
			} else {
				// redirecciono a la pagina de loguo
				response.sendRedirect("login.html");
			}
		} catch (Exception e) {
			// redirecciono a la pagina de loguo
			response.sendRedirect("login.html");
		}
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// NOTE: se guardan de acuerdo a la cantidad de usuarios los paramnetros
		// ingresados inicialmente en un tabla clave, valor
		super.init(config);

		logins = new Hashtable<String, Usuario>();

		// Usuario unUsuario = new Usuario(); ya no hace falta..

		Integer cantidadUsuarios = Integer.parseInt(config.getInitParameter("cantidadUsuarios"));
		String nombre;
		Usuario[] UsuariosDelSistema = new Usuario[cantidadUsuarios];

		// for (int i = 1; i <= cantidadUsuarios; i++) {
		for (int i = 0; i <= cantidadUsuarios - 1; i++) {
			int indice = i + 1;
			nombre = config.getInitParameter("usuario" + indice);
			System.out.println("holas");
			UsuariosDelSistema[i] = new UsuarioComun();
			UsuariosDelSistema[i].setNombreUsuario(nombre);
			UsuariosDelSistema[i].setPassword(config.getInitParameter("pass" + indice));
			UsuariosDelSistema[i].setPerfil(config.getInitParameter("perfil" + indice));

			logins.put(nombre, UsuariosDelSistema[i]);
		}
		config.getServletContext().setAttribute("usuariosActivos", logins); // PROBANDO!!
		// this.getServletConfig().getServletContext().setAttribute("usuariosActivos",
		// logins); ESTO ANDA!!!!!

		/*
		 * ESTE FOR NO ANDA,, en el objeto unUsuario se van pisando los valores
		 * asignados.. y todos los usuarios del HASH quedan con el valor del ultimo
		 * usuario.. se tiene que probar otra solucion, estaba viendo de implementar un
		 * arreglo pero se me complico un poco.... =)
		 * 
		 * Integer cantidadUsuarios =
		 * Integer.parseInt(config.getInitParameter("cantidadUsuarios"));
		 * 
		 * Usuario[] UsuariosDelSistema = new Usuario[cantidadUsuarios];
		 * 
		 * String nombre;
		 * 
		 * for (int i = 1; i <= cantidadUsuarios; i++) { nombre =
		 * config.getInitParameter("usuario"+i); unUsuario.setNombre(nombre);
		 * unUsuario.setContrasenia(config.getInitParameter("pass"+i));
		 * unUsuario.setPerfil(config.getInitParameter("perfil"+i));
		 * 
		 * logins.put(nombre, unUsuario); }
		 */

	}

}
