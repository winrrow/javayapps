package jyaa2018.grupo15.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioComun;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Register() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nom = request.getParameter("name");
		request.getParameter("email");
		request.getParameter("dni");
		request.getParameter("residence");
		request.getParameter("birthdate");
		request.getParameter("sex");
		String user = request.getParameter("username");
		String pass = request.getParameter("password");

		Hashtable<String, Usuario> logins;
		Object hash = request.getServletContext().getAttribute("usuariosActivos"); // PROBANDO!
		// Object hash =
		// this.getServletConfig().getServletContext().getAttribute("usuariosActivos");
		// ESTO ANDA!!
		logins = (Hashtable<String, Usuario>) (hash);

		Usuario unUsuario = new UsuarioComun();

		unUsuario.setNombreUsuario(user);
		unUsuario.setPassword(pass);
		unUsuario.setPerfil("USUARIO");

		logins.put(user, unUsuario);

		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		out.println("<HTML>");
		out.println("<HEAD>");
		out.println("<TITLE> Bienvenido!! </TITLE>");
		out.println("</HEAD>");
		out.println("<BODY>");
		out.println("<CENTER> <H1> Bienvenido a nuestra pagina web! </H1> </CENTER>");
		out.println("<p> Hola, " + nom + "."
				+ "Si en 5 segundos no es redireccionado al login, por favor oprima <a href=\"http:\\\\localhost:8080\\RutasArgentinas\\login.html\"> Aqui </a></p>");

		out.println("<script type=\"text/javascript\">");
		out.println(
				"function redireccionar(){window.location.href=\"http://localhost:8080/RutasArgentinas/login.html\";}");
		out.println("window.setTimeout (\"redireccionar()\", 5000); //tiempo expresado en milisegundos");
		out.println("</script>");
		out.println("</BODY>");
		out.println("</HTML>");
		out.close();

	}

}
