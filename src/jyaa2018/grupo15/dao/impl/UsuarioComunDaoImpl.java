package jyaa2018.grupo15.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import genericDaos.AbstractGenericDaos;
import jyaa2018.grupo15.daoNOSEUSAMAS.UsuarioDao;
import jyaa2018.grupo15.helpers.ConexionSingleton;
import jyaa2018.grupo15.models.Actividad;
import jyaa2018.grupo15.models.Punto;
import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioComun;

public class UsuarioComunDaoImpl extends AbstractGenericDaos<UsuarioComun> /*implements UsuarioDao*/ {

	public UsuarioComunDaoImpl(Class<UsuarioComun> persistentClass) {
		super(persistentClass);
		// TODO Auto-generated constructor stub
	}

	
	public List<UsuarioComun> getItems() {
		EntityManager em = ConexionSingleton.getEntityManager();
		List<UsuarioComun> resultList = (List<UsuarioComun>) (em.createQuery("from Usuario u where u.class='UsuarioComun'"))
				.getResultList();
		return resultList;
	}
	
	public Usuario checkUserPassword(String nombre, String pass) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Query q= (em.createQuery("from Usuario u where u.nombreUsuario=:nombre and u.password=:pass", Usuario.class));
		
		etx.commit();
		q.setParameter("nombre", nombre);
		q.setParameter("pass", pass);
		
		List<Usuario> resultList = q.getResultList();
		Usuario u = null;
	    if(resultList != null && !resultList.isEmpty()){		    	
	    	u = (Usuario) resultList.get(0);
	    	em.close();
			return u;
	    }
	    em.close();
		return u;
	
		/*Usuario u = (Usuario) q.getResultList().get(0);
		em.close();
		return u;*/
	}
	
	public Usuario checkUserExist(String nombre) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Query q= (em.createQuery("from Usuario u where u.nombreUsuario=:nombre", Usuario.class));
		
		etx.commit();
		q.setParameter("nombre", nombre);
		
		List<Usuario> resultList = q.getResultList();
		Usuario u = null;
	    if(resultList != null && !resultList.isEmpty()){		    	
	    	u = (Usuario) resultList.get(0);
	    	em.close();
			return u;
	    }
	    em.close();
		return u;
	
		/*Usuario u = (Usuario) q.getResultList().get(0);
		em.close();
		return u;*/
	}
	
	public Usuario checkUserEnabled(String nombre) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Query q = (em.createQuery("from Usuario u where u.nombreUsuario=:nombre and u.habilitado=:habilitacion", Usuario.class));
		
		etx.commit();
		q.setParameter("nombre", nombre);
		q.setParameter("habilitacion", true);
	
		List<Usuario> resultList = q.getResultList();
		Usuario u = null;
	    if(resultList != null && !resultList.isEmpty()){		    	
	    	u = (Usuario) resultList.get(0);
	    	em.close();
			return u;
	    }
	    em.close();
		return u;
		/*Usuario u = (Usuario) q.getResultList().get(0);
		em.close();
		return u;*/
	}
	
	@Override
	public void deleteById(Long id) {
		UsuarioComun item = getItem(id);
		item.setHabilitado(false);
		update(item);
	}
	
/*
	@Override
	public Usuario getItem(BigInteger id) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Usuario item= em.find(Usuario.class, id);
		etx.commit();
		em.close();
		return item;
	}
	
	@Override
	public Usuario checkUser(String nombre, String pass) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Query q= (em.createQuery("from Usuario u where u.nombreUsuario=:nombre and u.password=:pass",Usuario.class));
		
		etx.commit();
		q.setParameter("nombre", nombre);
		q.setParameter("pass", pass);
	
		Usuario u = (Usuario) q.getResultList().get(0);
		em.close();
		return u;
	}
	
	@Override
	public void guardarUsuario(Usuario u) {
		u.setHabilitado(true);
		EntityManager em = ConexionSingleton.getEntityManager();
		// ItemPresupuesto item = new ItemPresupuesto();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.persist((UsuarioComun) u );
		etx.commit();
		em.close();
	}

	@Override
	public void modificarUsuario(Usuario item) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.merge(item);
		etx.commit();
		em.close();
	}

	@Override
	public void borrarUsuario(Long id) {
		Usuario item= getItem(id);
		item.setHabilitado(false);
		modificarUsuario(item);
	}

	@Override
	public Usuario getItem(Long id) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Usuario item= em.find(Usuario.class, id);
		etx.commit();
		em.close();
		return item;
	}*/



}
