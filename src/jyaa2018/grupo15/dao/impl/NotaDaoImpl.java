package jyaa2018.grupo15.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import genericDaos.AbstractGenericDaos;
import jyaa2018.grupo15.daoNOSEUSAMAS.NotaDao;
import jyaa2018.grupo15.helpers.ConexionSingleton;
import jyaa2018.grupo15.models.Nota;

public class NotaDaoImpl extends AbstractGenericDaos<Nota> { //implements NotaDao{

	public NotaDaoImpl(Class<Nota> persistentClass) {
		super(persistentClass);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Nota> getItems() {
		EntityManager em = ConexionSingleton.getEntityManager();
		List<Nota> resultList = (List<Nota>) (em.createQuery("from Nota n order by a.nombre asc"))
				.getResultList();
		return resultList;
	}

	@Override
	public Nota getItem(Long id) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Nota item= em.find(Nota.class, id);
		etx.commit();
		em.close();
		return item;
	}

	/*@Override
	public void guardarNota(Nota n) {
		EntityManager em = ConexionSingleton.getEntityManager();
		// ItemPresupuesto item = new ItemPresupuesto();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.persist(n);
		etx.commit();
		em.close();
	}

	@Override
	public void modificarNota(Nota item) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.merge(item);
		etx.commit();
		em.close();
	}*/


}
