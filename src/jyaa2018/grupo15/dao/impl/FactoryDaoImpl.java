package jyaa2018.grupo15.dao.impl;

import jyaa2018.grupo15.daoNOSEUSAMAS.FactoryDao;
import jyaa2018.grupo15.models.Actividad;
import jyaa2018.grupo15.models.Calificacion;
import jyaa2018.grupo15.models.Dificultad;
import jyaa2018.grupo15.models.Nota;
import jyaa2018.grupo15.models.Punto;
import jyaa2018.grupo15.models.Ruta;
import jyaa2018.grupo15.models.UsuarioAdministrador;
import jyaa2018.grupo15.models.UsuarioComun;

public class FactoryDaoImpl implements FactoryDao {
	
	@Override
	public ActividadDaoImpl getActividadDAO(){
		return new ActividadDaoImpl(Actividad.class);
	}
	
	@Override
	public DificultadDaoImpl getDificultadDAO(){
		return new DificultadDaoImpl(Dificultad.class);
	}
	
	@Override
	public CalificacionDaoImpl getCalificacionDAO(){
		return new CalificacionDaoImpl(Calificacion.class);
	}

	@Override
	public NotaDaoImpl getNotaDao() {
		return new NotaDaoImpl(Nota.class);
	}

	@Override
	public RutaDaoImpl getRutaDao() {
		return new RutaDaoImpl(Ruta.class);
	}

	@Override
	public UsuarioComunDaoImpl getUsuarioComunDao() {
		return (UsuarioComunDaoImpl) new UsuarioComunDaoImpl(UsuarioComun.class);
	}
	
	@Override
	public UsuarioAdministradorDaoImpl getUsuarioAdministradorDao() {
		return (UsuarioAdministradorDaoImpl) new UsuarioAdministradorDaoImpl(UsuarioAdministrador.class);
	}

}
