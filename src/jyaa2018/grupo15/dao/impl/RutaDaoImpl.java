package jyaa2018.grupo15.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import genericDaos.AbstractGenericDaos;
import jyaa2018.grupo15.daoNOSEUSAMAS.RutaDao;
import jyaa2018.grupo15.helpers.ConexionSingleton;
import jyaa2018.grupo15.models.Punto;
import jyaa2018.grupo15.models.Ruta;
import jyaa2018.grupo15.models.Usuario;

public class RutaDaoImpl extends AbstractGenericDaos<Ruta> { //implements RutaDao {

	public RutaDaoImpl(Class<Ruta> persistentClass) {
		super(persistentClass);
		// TODO Auto-generated constructor stub
	}

	public List<Ruta> getRutasDeUsuario(Long id) {
		entityManager = conexion.getEntityManager();
		EntityTransaction etx = entityManager.getTransaction();
		etx.begin();
		Query q = (entityManager.createQuery("from Ruta r join Usuario_Ruta ur on r.id = ur.ruta_ where ur.Usuario_id=:id", Ruta.class));
		q.setParameter("id", id);
		etx.commit();
		List<Ruta> r = (List<Ruta>) q.getResultList();
		entityManager.close();
		return r;
	}
	
	public List<Ruta> getUltimasRutas() {
		entityManager = conexion.getEntityManager();
		EntityTransaction etx = entityManager.getTransaction();
		etx.begin();
		Query q = (entityManager.createQuery("from Ruta", Ruta.class));
		// q.setMaxResults(10);
		etx.commit();
		List<Ruta> r = (List<Ruta>) q.getResultList();
		entityManager.close();
		return r;	
	}
	
	public List<Ruta> getRutasDeActividad(Long id) {
		entityManager = conexion.getEntityManager();
		((EntityTransaction) entityManager.getTransaction()).begin(); 
		Query q = (entityManager.createQuery("from Ruta r where str(r.actividad) like :id", Ruta.class));
		q.setParameter("id", id.toString());
		List<Ruta> r = (List<Ruta>) q.getResultList();		
		((EntityTransaction) entityManager.getTransaction()).commit();
		entityManager.close();
		return r;
	}
	
	@Override
	public void deleteById(Long id) {
		Ruta item = getItem(id);
		item.setHabilitado(false);
		update(item);

	}
	
	/*@// TODO Auto-generated method stub
	Override
	public List<Ruta> getItems() {
		EntityManager em = ConexionSingleton.getEntityManager();
		List<Ruta> resultList = (List<Ruta>) (em.createQuery("from Ruta r order by r.nombre asc")).getResultList();
		return resultList;
	}*/

	/*@Override
	public void guardarRuta(Ruta r) {
		EntityManager em = ConexionSingleton.getEntityManager();
		// ItemPresupuesto item = new ItemPresupuesto();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.persist(r);
		etx.commit();
		em.close();
	}

	@Override
	public void modificarRuta(Ruta item) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.merge(item);
		etx.commit();
		em.close();
	}*/

	/*EntityManager em = ConexionSingleton.getEntityManager();
	EntityTransaction etx = em.getTransaction();
	etx.begin();
	Query q = (em.createQuery("from Ruta r where str(r.actividad) like :id", Ruta.class));
	q.setParameter("id", id.toString());
	etx.commit();
	List<Ruta> r = (List<Ruta>) q.get;
	em.close();
	return r;*/
	

}
