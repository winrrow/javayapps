package jyaa2018.grupo15.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import genericDaos.AbstractGenericDaos;
import jyaa2018.grupo15.daoNOSEUSAMAS.UsuarioDao;
import jyaa2018.grupo15.helpers.ConexionSingleton;
import jyaa2018.grupo15.models.Actividad;
import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioAdministrador;
import jyaa2018.grupo15.models.UsuarioComun;

public class UsuarioAdministradorDaoImpl extends AbstractGenericDaos<UsuarioAdministrador>{// implements UsuarioDao {

	public UsuarioAdministradorDaoImpl(Class<UsuarioAdministrador> persistentClass) {
		super(persistentClass);
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<UsuarioAdministrador> getItems() {
		EntityManager em = ConexionSingleton.getEntityManager();
		List<UsuarioAdministrador> resultList = (List<UsuarioAdministrador>) (em.createQuery("from Usuario u where u.class='UsuarioAdministrador'"))
				.getResultList();
		return resultList;
	}
/*
	@Override
	public UsuarioAdministrador getItem(Long id) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		UsuarioAdministrador item = em.find(UsuarioAdministrador.class, id);
		etx.commit();
		em.close();
		return item;
	}

	@Override
	public void guardarUsuario(Usuario u) {
		u.setHabilitado(true);
		EntityManager em = ConexionSingleton.getEntityManager();
		// ItemPresupuesto item = new ItemPresupuesto();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.persist(u);
		etx.commit();
		em.close();
	}

	@Override
	public void modificarUsuario(Usuario item) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		em.merge(item);
		etx.commit();
		em.close();
	}

	@Override
	public void borrarUsuario(Long id) {
		Usuario item = getItem(id);
		item.setHabilitado(false);
		modificarUsuario(item);
	}

	public void borrarUsuarioPorUsuario(UsuarioAdministrador u) {
		this.modificarUsuario(u);
	}

	@Override
	public Usuario getItem(BigInteger id) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Usuario item = em.find(Usuario.class, id);
		etx.commit();
		em.close();
		return item;
	}

	@Override
	public Usuario checkUser(String nombre, String pass) {
		// TODO Auto-generated method stub
		return null;
	}*/
	
	@Override
	public void deleteById(Long id) {
		UsuarioAdministrador item = getItem(id);
		item.setHabilitado(false);
		update(item);
	}

}
