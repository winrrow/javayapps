package jyaa2018.grupo15.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import genericDaos.AbstractGenericDaos;
import jyaa2018.grupo15.daoNOSEUSAMAS.ActividadDao;
import jyaa2018.grupo15.helpers.ConexionSingleton;
import jyaa2018.grupo15.models.Actividad;

public class ActividadDaoImpl extends AbstractGenericDaos<Actividad> {//implements ActividadDao {

	
	public List<Actividad> getActividadesHabilitadas() {
		EntityManager em = ConexionSingleton.getEntityManager();
		List<Actividad> resultList = (List<Actividad>) (em.createQuery("from Actividad a where habilitado=true order by a.nombre asc"))
				.getResultList();
		return resultList;
	}
	
	public ActividadDaoImpl(Class<Actividad> persistentClass) {
		super(persistentClass);
		// TODO Auto-generated constructor stub
	}
	
	 
	// borrado logico (SE HACE EN EL UPDATE) todo el atributo de la entidad y lo persisto
	
	//borrado fisico em.remove --> SOLO SE PERMITE SI NO HAY RUTAS ASOCIADAS A LA ACTIVIDAD
	@Override
	public void deleteById(Long id) {
		Actividad item = getItem(id);
		//item.setHabilitado(false);
		delete(item);
	}
}
