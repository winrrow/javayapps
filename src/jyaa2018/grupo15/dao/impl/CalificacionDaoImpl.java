package jyaa2018.grupo15.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import genericDaos.AbstractGenericDaos;
import jyaa2018.grupo15.helpers.ConexionSingleton;
import jyaa2018.grupo15.models.Calificacion;

public class CalificacionDaoImpl extends AbstractGenericDaos<Calificacion> { //implements CalificacionDao{
	
	public CalificacionDaoImpl(Class<Calificacion> persistentClass) {
		super(persistentClass);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public List<Calificacion> getItems() {
		EntityManager em = ConexionSingleton.getEntityManager();
		List<Calificacion> resultList = (List<Calificacion>) (em.createQuery("from Calificacion c order by a.nombre asc"))
				.getResultList();
		return resultList;
	}
	
	@Override
	public Calificacion getItem(Long id) {
		EntityManager em = ConexionSingleton.getEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		Calificacion item= em.find(Calificacion.class, id);
		etx.commit();
		em.close();
		return item;
	}
	
}
