package rest;

/*
@Path("/fotos")
@Consumes(value= MediaType.APPLICATION_JSON)
@Produces(value = MediaType.APPLICATION_JSON)
public class FotosService {		
	
	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	private FactoryDao factory = new FactoryDaoImpl();

	private String mensaje;
		
		@GET
		public List<Foto> getFotos() {
			FotoDaoImpl d = (FotoDaoImpl) factory.getFotoDAO();
			return d.getItems();
		}
				
		@POST
		public Response createFoto(Foto fotoRequest) {
			FotoDaoImpl d = factory.getFotoDAO();
			d.save(fotoRequest);
			Foto foto = d.getItem(fotoRequest.getId());
			if (foto != null) {
				return Response.ok().entity(foto).build();
			} else {
				mensaje = "No se pudo crear la foto";
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			}			
		}
		
		@PUT
		public Response updateFoto(Foto fotoRequest) {
			FotoDaoImpl d = factory.getFotoDAO();
			Foto aux = d.getItem(fotoRequest.getId());
			if (aux != null) {
				d.update(fotoRequest);
				return Response.ok().entity(fotoRequest).build();
				//return Response.ok(fotoRequest).build();
			} else {
				mensaje = "Foto no encontrada";
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			}			
		}
		
		@DELETE
		@Path("{fotoId}")
		public Response deleteFoto( @PathParam("fotoId") long idFoto) {
			System.out.println("fotoId ==> " + idFoto);
			
			FotoDaoImpl d = factory.getFotoDAO();
			Foto aux = d.getItem(idFoto);
			if (aux != null) {
				d.deleteById(idFoto);
				return Response.noContent().build();
			} else {
				mensaje = "No existe la foto con ese id";
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			}
		}
		
		
		@HEAD
		public Response pingFotosService() {
			return Response.noContent().header("running", true).build();
		}
		
}*/
