package rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import jyaa2018.grupo15.dao.impl.ActividadDaoImpl;
import jyaa2018.grupo15.dao.impl.FactoryDaoImpl;
import jyaa2018.grupo15.dao.impl.RutaDaoImpl;
import jyaa2018.grupo15.daoNOSEUSAMAS.ActividadDao;
import jyaa2018.grupo15.daoNOSEUSAMAS.FactoryDao;
import jyaa2018.grupo15.helpers.RespuestaJSON;
import jyaa2018.grupo15.models.Actividad;
import jyaa2018.grupo15.models.Ruta;

@Path("/actividades")
@Consumes(value = MediaType.APPLICATION_JSON)
@Produces(value = MediaType.APPLICATION_JSON)
public class ActividadesService {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	private FactoryDao factory = new FactoryDaoImpl();
	private String mensaje;

	@GET
	// @Produces(MediaType.APPLICATION_JSON)
	public List<Actividad> getActividades() {
		ActividadDaoImpl d = factory.getActividadDAO();
		return d.getItems();
	}

	@GET
	@Path("/habilitadas")
	// @Produces(MediaType.APPLICATION_JSON)
	public Response getActividadesHabilitadas() {
		ActividadDaoImpl d = factory.getActividadDAO();
		
		RespuestaJSON resul = new RespuestaJSON();
		resul.Ok = true;
		resul.Value = d.getActividadesHabilitadas();
		resul.Message = "Se obtuvieron las rutas del usuario.";			
		
		return Response.status(Response.Status.ACCEPTED).entity(resul).build();
		//return d.getActividadesHabilitadas();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getActividadPorId(@PathParam("id") Long id) {
		ActividadDaoImpl d = factory.getActividadDAO();
		Actividad actividad = d.getItem(id);
		if (actividad != null) {
			return Response.ok().entity(actividad).build();
		} else {
			mensaje = "No se encontr� la actividad";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}
	//

	@POST
	@Path("/guardarActividad")
	/*
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON)
	 */
	public Response createActividad(Actividad actividadRequest) {
		ActividadDaoImpl d = factory.getActividadDAO();
		d.save(actividadRequest);
		Actividad activity = d.getItem(actividadRequest.getId());
		if (activity != null) {
			return Response.ok().entity(activity).build();
		} else {
			mensaje = "No se pudo crear la actividad";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateActividad(Actividad actividadRequest) {
		ActividadDaoImpl d = factory.getActividadDAO();
		Actividad aux = d.getItem(actividadRequest.getId());
		if (aux != null) {
			d.update(actividadRequest);
			return Response.ok().entity(actividadRequest).build();
		} else {
			mensaje = "Actividad no encontrada";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}

	/*
	 * 
	 * EN LA TEORIA, AL LLAMAR AL BORRAR, NO BORRA POR ID,, sino que borra por
	 * elemento Usuario aux = udao.read(id); if (aux != null){ udao.delete(aux);
	 * 
	 */

	@DELETE
	@Path("/borrarActividad/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteActividad(@PathParam("id") Long id) {
		ActividadDaoImpl d = factory.getActividadDAO();
		Actividad aux = d.getItem(id);
		if (aux != null) {
			// TENGO QUE CHEQUEAR QUE NO HAYA RUTAS ASOCIADAS A LA ACTIVIDAD
			RutaDaoImpl r = factory.getRutaDao();
			List<Ruta> listaDeRutas = r.getRutasDeActividad(id);
			if (listaDeRutas.size() > 0) {
				mensaje = "La actividad posee rutas asociadas y no se puede eliminar";
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			} else { // la elimino Fisicamente
				d.deleteById(id);
				return Response.noContent().build();
			}
		} else {
			mensaje = "No existe la actividad con ese id";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}

	@HEAD
	public Response pingActividadesService() {
		return Response.noContent().header("running", true).build();
	}

}
