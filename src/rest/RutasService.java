package rest;

import java.util.List;

import javax.smartcardio.ResponseAPDU;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.hibernate.HibernateException;

import jyaa2018.grupo15.dao.impl.FactoryDaoImpl;
import jyaa2018.grupo15.dao.impl.RutaDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioAdministradorDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioComunDaoImpl;
import jyaa2018.grupo15.daoNOSEUSAMAS.ActividadDao;
import jyaa2018.grupo15.daoNOSEUSAMAS.FactoryDao;
import jyaa2018.grupo15.daoNOSEUSAMAS.RutaDao;
import jyaa2018.grupo15.daoNOSEUSAMAS.UsuarioDao;
import jyaa2018.grupo15.helpers.RespuestaJSON;
import jyaa2018.grupo15.models.Actividad;
import jyaa2018.grupo15.models.Ruta;
import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioAdministrador;
import jyaa2018.grupo15.models.UsuarioComun;

@Path("/rutas")
@Consumes(value = MediaType.APPLICATION_JSON)
@Produces(value = MediaType.APPLICATION_JSON)
public class RutasService {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	private FactoryDao factory = new FactoryDaoImpl();
	private String mensaje;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Ruta> getRutas() {
		RutaDaoImpl d = (RutaDaoImpl) factory.getRutaDao();
		return d.getItems();
	}
	
	@GET
	@Path("/ultimas")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Ruta> getUltimasRutas() {
		RutaDaoImpl d = (RutaDaoImpl) factory.getRutaDao();
		return d.getUltimasRutas();
	}

	@GET
	@Path("/deUsuario/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRutasDeUsuario(@PathParam("id") Long id) {
		RutaDaoImpl d = (RutaDaoImpl) factory.getRutaDao();
		List<Ruta> rutas;
		/*ruta = d.getRutasDeUsuario(id);
		return Response.status(Response.Status.ACCEPTED).entity(ruta).build();*/
		try {
			rutas= d.getUltimasRutas();
			
			RespuestaJSON resul = new RespuestaJSON();
			resul.Ok = true;
			resul.Value = rutas;
			resul.Message = "Se obtuvieron las rutas del usuario.";			
			
			return Response.status(Response.Status.ACCEPTED).entity(resul).build();
		/*} catch (HibernateException hibernateEx) {
			mensaje = "Ocurrio un error al consultar las rutas del usuario";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();		
		}*/
		} catch (Exception Ex) {
		 
			RespuestaJSON prueba = new RespuestaJSON();
			prueba.Ok = false;
			prueba.Value = null;
			mensaje = "Ocurrio un error al consultar las rutas del usuario";
			prueba.Message = mensaje;
			return Response.status(Response.Status.ACCEPTED).entity(prueba).build();		
		}
		/*if (ruta != null) {
			return Response.ok().entity(ruta).build();
		} else {
			mensaje = "No se encontraron rutas para este usuario";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}*/
	}
	
	  @POST	  
	  @Path("/guardarRuta")
	  /*@Produces(MediaType.APPLICATION_JSON)	  
	  @Consumes(MediaType.APPLICATION_JSON) */
	  public Response createRuta(Ruta rutaRequest) { 
		  RutaDaoImpl d = factory.getRutaDao(); 
		  d.save(rutaRequest);
		  Ruta route = d.getItem(rutaRequest.getId()); 
		  if (route != null) { 
			  return Response.ok().entity(route).build(); 
			  } 
		  else { 
			  mensaje = "No se pudo guardar la nueva ruta"; 
			  return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();		  
		  	} 
	  }
	  
	  @PUT
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		public Response updateRuta(Ruta rutaRequest) {
		  RutaDaoImpl d = factory.getRutaDao(); 
		  d.save(rutaRequest);
			Ruta aux = d.getItem(rutaRequest.getId());
			if (aux != null) {
				d.update(rutaRequest);
				return Response.ok().entity(rutaRequest).build();
			} else {
				mensaje = "Ruta no encontrada";
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			}
		}
	 
	  @DELETE
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response deleteRuta(@PathParam("id") Long id) {
			RutaDaoImpl d = factory.getRutaDao();
			Ruta aux = d.getItem(id);
			if (aux != null) {
				d.deleteById(id);
				return Response.noContent().build();
			} else {
				mensaje = "No existe la actividad con ese id";
				return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
			}
		}
		
		@HEAD
		public Response pingActividadesService() {
			return Response.noContent().header("running", true).build();
		}
}
