package rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import jyaa2018.grupo15.dao.impl.DificultadDaoImpl;
import jyaa2018.grupo15.dao.impl.FactoryDaoImpl;
import jyaa2018.grupo15.dao.impl.RutaDaoImpl;
import jyaa2018.grupo15.daoNOSEUSAMAS.FactoryDao;
import jyaa2018.grupo15.models.Dificultad;
import jyaa2018.grupo15.models.Ruta;


@Path("/dificultades")
@Consumes(value = MediaType.APPLICATION_JSON)
@Produces(value = MediaType.APPLICATION_JSON)
public class DificultadService {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	private FactoryDao factory = new FactoryDaoImpl();
	private String mensaje;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dificultad> getDificultadesRutas() {
		DificultadDaoImpl d = (DificultadDaoImpl) factory.getDificultadDAO();
		return d.getItems();
	}
}