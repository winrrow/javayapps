package rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import jyaa2018.grupo15.dao.impl.FactoryDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioAdministradorDaoImpl;
import jyaa2018.grupo15.dao.impl.UsuarioComunDaoImpl;
import jyaa2018.grupo15.daoNOSEUSAMAS.ActividadDao;
import jyaa2018.grupo15.daoNOSEUSAMAS.FactoryDao;
import jyaa2018.grupo15.daoNOSEUSAMAS.UsuarioDao;
import jyaa2018.grupo15.helpers.RespuestaJSON;
import jyaa2018.grupo15.models.Actividad;
import jyaa2018.grupo15.models.Usuario;
import jyaa2018.grupo15.models.UsuarioAdministrador;
import jyaa2018.grupo15.models.UsuarioComun;

@Path("/usuarios")
public class Usuarios {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	private FactoryDao factory = new FactoryDaoImpl();

	private String mensaje;

	@GET
	@Path("/comunes")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UsuarioComun> getUsuariosComunes() {
		UsuarioComunDaoImpl d = (UsuarioComunDaoImpl) factory.getUsuarioComunDao();
		return d.getItems();
	}

	@GET
	@Path("/administradores")
	@Produces(MediaType.APPLICATION_JSON)
	public List<UsuarioAdministrador> getUsuariosAdministradores() {
		UsuarioAdministradorDaoImpl d = (UsuarioAdministradorDaoImpl) factory.getUsuarioAdministradorDao();
		return d.getItems();
	}
	
	/* 14/11/2020 cambio esto por un Response.. as� si el usuario esta deshabilitado lo puedo informar
	 * @GET
	@Path("/login/{nombre}/{pass}")
	@Produces(MediaType.APPLICATION_JSON)
	public Usuario checkLogin(@PathParam("nombre") String nombre, @PathParam("pass") String pass) {
		UsuarioComunDaoImpl d = (UsuarioComunDaoImpl) factory.getUsuarioComunDao();
		return d.checkUser(nombre, pass);
	}*/
	
	
	@GET
	@Path("/login/{nombre}/{pass}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkLogin(@PathParam("nombre") String nombre, @PathParam("pass") String pass) {		
		String mensaje = "";
		Usuario unUsuario;
		RespuestaJSON resul = new RespuestaJSON();
		
		// tengo que verificar si el usuario existe
		UsuarioComunDaoImpl d1 = (UsuarioComunDaoImpl) factory.getUsuarioComunDao();
		unUsuario = d1.checkUserExist(nombre);
		if (unUsuario != null) {
			UsuarioComunDaoImpl d2 = (UsuarioComunDaoImpl) factory.getUsuarioComunDao();
			unUsuario = d2.checkUserPassword(nombre, pass); // verifico que la contrase�a sea correcta
	
			if (unUsuario != null) {
				// tengo que verificar si el usuario est� habilitado
				UsuarioComunDaoImpl d3 = (UsuarioComunDaoImpl) factory.getUsuarioComunDao();
				unUsuario = d3.checkUserEnabled(nombre); 
				if (unUsuario != null) {
					mensaje = "Login de usuario correcto!."; 
					resul.Ok = true;
				}
				else
				{
					mensaje = "El usuario fue deshabilitado por un administrador."; 
					resul.Ok = false;
				}
			}
			else
			{
				mensaje = "La contrase�a ingresada es incorrecta."; 
				resul.Ok = false;
			}			
		}
		else
		{
			mensaje = "El Usuario ingresado no existe."; 
			resul.Ok = false;
		}
				
		resul.Value = unUsuario;
		resul.Message = mensaje;			
		
		return Response.status(Response.Status.ACCEPTED).entity(resul).build();
	}
	
	
	/*			
	@GET
	@Path("comun/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response encontrarComun(@PathParam("id") Long id) {
		UsuarioDao d = factory.getUsuarioComunDao();
		Usuario usuario = d.getItem(id);
		if (usuario != null) {
			return Response.ok().entity(usuario).build();
		} else {
			mensaje = "No se encontr� el usuario comun";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}
	*/


	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response encontrarAdmin(@PathParam("id") Long id) {
		UsuarioAdministradorDaoImpl d = factory.getUsuarioAdministradorDao();
		Usuario usuario = d.getItem(id);
		if (usuario != null) {
			return Response.ok().entity(usuario).build();
		} else {
			mensaje = "No se encontr� el usuario";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}

	@POST
	@Path("/comunes")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crearComun(UsuarioComun u) {
		UsuarioComunDaoImpl d = factory.getUsuarioComunDao();
		d.save(u);
		Usuario user = d.getItem(u.getId());
		if (user != null) {
			return Response.ok().entity(user).build();
		} else {
			mensaje = "No se pudo crear el usuario";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}
	
	@POST
	@Path("/administradores")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crearAdmin(UsuarioAdministrador u) {
		UsuarioAdministradorDaoImpl d = factory.getUsuarioAdministradorDao();
		d.save(u);
		Usuario user = d.getItem(u.getId());
		if (user != null) {
			return Response.ok().entity(user).build();
		} else {
			mensaje = "No se pudo crear el usuario administrador";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}
	
	@PUT
	@Path("/comunes")   
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editar(UsuarioComun u) {
		UsuarioComunDaoImpl d = factory.getUsuarioComunDao();
		Usuario aux = d.getItem(u.getId());
		if (aux != null) {
			d.update(u);
			return Response.ok().entity(u).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("[]").build();
		}
	}
	
	@PUT
	@Path("/administradores")   
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editar(UsuarioAdministrador u) {
		UsuarioAdministradorDaoImpl d = factory.getUsuarioAdministradorDao();
		Usuario aux = d.getItem(u.getId());
		if (aux != null) {
			d.update(u);
			return Response.ok().entity(u).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("[]").build();
		}
	}
	
	@DELETE
	@Path("/comunes/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response borrarComun(@PathParam("id") Long id) {
		UsuarioComunDaoImpl d = factory.getUsuarioComunDao();
		Usuario aux = d.getItem(id);
		if (aux != null) {
			d.deleteById(id);
			return Response.noContent().build();
		} else {
			mensaje = "No existe el usuario con ese id";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}

	@DELETE
	@Path("/administradores/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response borrarAdmin(@PathParam("id") Long id) {
		UsuarioAdministradorDaoImpl d = factory.getUsuarioAdministradorDao();
		Usuario aux = d.getItem(id);
		if (aux != null) {
			d.deleteById(id);
			return Response.noContent().build();
		} else {
			mensaje = "No existe el usuario con ese id";
			return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
		}
	}
}
