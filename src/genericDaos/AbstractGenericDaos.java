package genericDaos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;

import jyaa2018.grupo15.helpers.ConexionNoSingleton;
import jyaa2018.grupo15.helpers.ConexionSingleton;

public abstract class AbstractGenericDaos<T>{

	protected Class<T> persistentClass;
	
	protected ConexionNoSingleton conexion= new ConexionNoSingleton();

    public AbstractGenericDaos(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    public T create() {
        T instance = null;
        try {
            instance = persistentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        } 

        return instance;
    }
	

	@PersistenceContext
	protected
	EntityManager entityManager = conexion.getEntityManager();

	public void setClazz(Class<T> clazzToSet) {
		this.persistentClass = clazzToSet;
	}

	public T getItem(final Long id) {		
		entityManager = conexion.getEntityManager();
		T obj = entityManager.find(persistentClass, id);
		entityManager.close();		  		
		return obj;		
	}	
	
	public List<T> getItems() {
		entityManager = conexion.getEntityManager();
		List<T> obj = entityManager.createQuery("from " + persistentClass.getName()).getResultList();
		entityManager.close();		  		
		return obj;	
	}
	
	
	public void save(T entity) {
		entityManager = conexion.getEntityManager();
		((EntityTransaction) entityManager.getTransaction()).begin(); 
		entityManager.persist(entity);
		((EntityTransaction) entityManager.getTransaction()).commit();
		entityManager.close();
	}
	

	public void update(T entity) {
		entityManager = conexion.getEntityManager();
		((EntityTransaction) entityManager.getTransaction()).begin(); 
		entityManager.merge(entity);
		((EntityTransaction) entityManager.getTransaction()).commit();
		entityManager.close();
	}

	public void delete(T entity) {
		entityManager = conexion.getEntityManager();
		((EntityTransaction) entityManager.getTransaction()).begin(); 
		//entityManager.remove(entity); se cambia por la linea de abajo..
		entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
		((EntityTransaction) entityManager.getTransaction()).commit();
		entityManager.close();
	}

	public void deleteById(Long entityId) {
		/*entityManager = conexion.getEntityManager();
		((EntityTransaction) entityManager.getTransaction()).begin(); 
		NO HACE FALTA, LLAMA A METODOS QUE LO HACEN*/
		T entity = getItem(entityId);
		delete(entity);
		/*((EntityTransaction) entityManager.getTransaction()).commit();
		entityManager.close();*/
	}

}