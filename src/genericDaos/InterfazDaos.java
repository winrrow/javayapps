package genericDaos;

import java.io.Serializable;
import java.util.List;

import jyaa2018.grupo15.models.Actividad;
import jyaa2018.grupo15.models.Calificacion;

public interface InterfazDaos<T extends Serializable> {

	abstract T getById(final long id);

	abstract T getItem(final Long id); // nuestro

	abstract List<T> findAll();

	abstract List<T> getItems(); // nuestro

	abstract void save(final T entity);

	abstract void guardar(final T entity); // nuestro

	abstract T update(final T entity);

	abstract T modificar(final T entity); // nuestro

	abstract void delete(final T entity);

	abstract void borrar(final T entity); // nuestro

	abstract void deleteById(final long entityId);

	// de Actividad
	/*
	 * public List<Actividad> getItems(); public Actividad getItem(Long id); public
	 * void guardarActividad(Actividad a); public void modificarActividad(Actividad
	 * a); public void borrarActividad(Long id);
	 */
	// de Clasificacion
	/*
	 * public List<Calificacion> getItems(); public Calificacion getItem(Long id);
	 * public void guardarCalificacion(Calificacion c); public void
	 * modificarCalificacion(Calificacion c); public void borrarCalificacion(Long
	 * id);
	 */

}
