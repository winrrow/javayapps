CREATE TABLE Dificultad(
ID int NOT NULL AUTO_INCREMENT,
Nombre varchar(255) NOT NULL,
 PRIMARY KEY (ID));
INSERT INTO Dificultad(Nombre) values ('FACIL');
INSERT INTO Dificultad(Nombre) values ('MODERADO');
INSERT INTO Dificultad(Nombre) values ('DIFICIL');
INSERT INTO Dificultad(Nombre) values ('MUY DIFICIL');
INSERT INTO Dificultad(Nombre) values ('SOLO EXPERTO');